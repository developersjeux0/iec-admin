package developersjeux.com.iec_admin.model;

public class GroupModel {
    private String Id, name, Image, AdminId;

    public GroupModel(String id, String name, String image, String adminId) {
        Id = id;
        this.name = name;
        Image = image;
        AdminId = adminId;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getAdminId() {
        return AdminId;
    }

    public void setAdminId(String adminId) {
        AdminId = adminId;
    }
}
