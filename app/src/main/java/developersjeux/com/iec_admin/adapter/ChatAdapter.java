package developersjeux.com.iec_admin.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import developersjeux.com.iec_admin.R;
import developersjeux.com.iec_admin.activity.ImageViewActivity;
import developersjeux.com.iec_admin.model.ChatModel;
import developersjeux.com.iec_admin.universal.IecPreferences;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {

    private Context context;
    private ArrayList<ChatModel> data = new ArrayList<>();
    private IecPreferences preferences;

    public ChatAdapter(Context context) {
        this.context = context;
        preferences = new IecPreferences(context);
    }

    public void setData(ArrayList<ChatModel> data) {
        this.data = data;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.message_row, parent, false);
        return new ChatViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChatViewHolder holder, int position) {
        final ChatModel model = data.get(position);

        //---------------Check where to place message, left or Right
        if (model.getSenderId().equals(preferences.getUserId())) { //Set Message to right
            holder.lnr_Message.setBackgroundResource(R.drawable.bg_chat_right);
            holder.lnr_Parent.setGravity(Gravity.END);
            holder.tv_Message.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else {//Set Message to left
            holder.lnr_Message.setBackgroundResource(R.drawable.bg_chat_left);
            holder.lnr_Parent.setGravity(Gravity.START);
            holder.tv_Message.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
        }

        //---------------Check if message os text or image
        if (model.getMessage().startsWith("http")) {//Message is Image
            holder.rl_Img.setVisibility(View.VISIBLE);
            holder.tv_Message.setVisibility(View.GONE);
            Glide.with(context).load(model.getMessage()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                    holder.iv_Message.setImageResource(R.drawable.ic_person_black_24dp);
                    holder.progressBar_Message.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    holder.progressBar_Message.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.iv_Message);
        } else {//Message is text
            holder.rl_Img.setVisibility(View.GONE);
            holder.tv_Message.setVisibility(View.VISIBLE);
            holder.tv_Message.setText(model.getMessage());
        }

        //-------------Handle click Listener on image
        holder.iv_Message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("CheckImageMessage", model.getMessage());
                context.startActivity(new Intent(context, ImageViewActivity.class)
                        .putExtra("IMAGE_LINK", model.getMessage())
                );
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ChatViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_Message;
        TextView tv_Message;
        LinearLayout lnr_Parent, lnr_Message;
        RelativeLayout rl_Img;
        ProgressBar progressBar_Message;

        ChatViewHolder(@NonNull View itemView) {
            super(itemView);
            lnr_Parent = itemView.findViewById(R.id.lnr_Parent);
            lnr_Message = itemView.findViewById(R.id.lnr_Message);
            tv_Message = itemView.findViewById(R.id.tv_Message);
            rl_Img = itemView.findViewById(R.id.rl_Img);
            iv_Message = itemView.findViewById(R.id.iv_Message);
            progressBar_Message = itemView.findViewById(R.id.progressBar_Message);
        }
    }
}
