package developersjeux.com.iec_admin.activity;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;

import developersjeux.com.iec_admin.R;

public class ImageViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image__view);
        ImageView Back_Button = findViewById(R.id.Back_Button_Id);
        ImageView Download_Button = findViewById(R.id.Download_Button_Id);

        final ImageView Main_Image_View = findViewById(R.id.Main_Image_View);
        final ProgressBar P_Bar = findViewById(R.id.P_Bar_Id);

        Intent Data = getIntent();
        if (Data != null) {
            final String Image_Link = Data.getStringExtra("IMAGE_LINK");
            Log.v("CheckImageMessage", Image_Link);
            Glide.with(ImageViewActivity.this)
                    .load(Image_Link)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            P_Bar.setVisibility(View.GONE);
                            Main_Image_View.setImageResource(R.drawable.ic_imeg_error);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object dd, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            P_Bar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(Main_Image_View);

            Download_Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (checkIfAlreadyhavePermission()) {
                        String filename = getResources().getString(R.string.app_name) + "_" + System.currentTimeMillis() + ".jpg";
                        String downloadUrlOfImage = Image_Link;
                        File direct =
                                new File(Environment
                                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                                        .getAbsolutePath() + "/" + getResources().getString(R.string.app_name) + "/");

                        if (!direct.exists()) {
                            direct.mkdir();
                        }

                        DownloadManager dm = (DownloadManager) ImageViewActivity.this.getSystemService(Context.DOWNLOAD_SERVICE);
                        Uri downloadUri = Uri.parse(downloadUrlOfImage);
                        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
                        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                                .setAllowedOverRoaming(false)
                                .setTitle(filename)
                                .setMimeType("image/jpeg")
                                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                                .setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES,
                                        File.separator + getResources().getString(R.string.app_name) + File.separator + filename);

                        dm.enqueue(request);
                    } else {
                        ActivityCompat.requestPermissions(ImageViewActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    }

                }
            });
        }


        Back_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    private boolean checkIfAlreadyhavePermission() {

        if (Build.VERSION.SDK_INT >= 23) {
            return (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        } else {
            return true;
        }
    }
}
