package developersjeux.com.iec_admin.model;

public class GroupMessageModel {
    private String senderId, senderName, groupId, message, timeStamp;

    public GroupMessageModel() {
    }

    public GroupMessageModel(String senderId, String senderName, String groupId, String message, String timeStamp) {
        this.senderId = senderId;
        this.senderName = senderName;
        this.groupId = groupId;
        this.message = message;
        this.timeStamp = timeStamp;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
