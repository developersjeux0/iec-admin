package developersjeux.com.iec_admin.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import developersjeux.com.iec_admin.R;
import developersjeux.com.iec_admin.adapter.ChatWithAllAdapter;
import developersjeux.com.iec_admin.model.ChatModel;
import developersjeux.com.iec_admin.model.UsersModel;
import developersjeux.com.iec_admin.universal.APIs;
import developersjeux.com.iec_admin.universal.AppController;
import developersjeux.com.iec_admin.universal.HelperClass;
import developersjeux.com.iec_admin.universal.IecPreferences;
import developersjeux.com.iec_admin.universal.VolleyMultipartRequest;

public class ChatWithAllActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout rl_Root;
    String currentUserId;
    private ImageView iv_Back, iv_Menu, iv_Attach;
    private ImageView iv_Send;
    private EditText et_Message;
    private RecyclerView rclView_Chat;
    private Uri resultUri;
    private IecPreferences preferences;
    private ArrayList<UsersModel> allUsers;
    private Context context;
    private ArrayList<ChatModel> previousChat = new ArrayList<>();
    private ChatWithAllAdapter chatAdapter;
    private ArrayList<String> msgIds = new ArrayList<>();
    private String TAG = "VOLLEY_LIBRARY";
    private Handler handler;
    private Runnable r;
    private Animation scale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_with_all);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorWhite));
        }

        init();
        linkViews();
        setupRecyclerView();
        setClickListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        handlerRequestsForGettingChats();
    }

    private void handlerRequestsForGettingChats() {
        try {
            handler = new Handler();
            r = new Runnable() {
                public void run() {
                    handler.postDelayed(this, 1000);
                    getAllChats();
                }
            };

            handler.postDelayed(r, 1000);
        } catch (Exception e) {
            handler.removeCallbacks(null);
        }

    }

    private void linkViews() {
        rl_Root = findViewById(R.id.rl_Root);
        iv_Menu = findViewById(R.id.iv_Menu);
        iv_Attach = findViewById(R.id.iv_Attach);
        iv_Send = findViewById(R.id.iv_Send);
        et_Message = findViewById(R.id.et_Message);
        rclView_Chat = findViewById(R.id.rclView_Chat);
        iv_Back = findViewById(R.id.iv_Back);
    }

    private void setClickListener() {
        iv_Menu.setOnClickListener(this);
        iv_Attach.setOnClickListener(this);
        iv_Send.setOnClickListener(this);
        iv_Back.setOnClickListener(this);
    }

    private void init() {
        preferences = new IecPreferences(ChatWithAllActivity.this);
        context = ChatWithAllActivity.this;
        Intent intent = getIntent();
        allUsers = (ArrayList<UsersModel>) intent.getSerializableExtra("ALL_USERS");
        scale = AnimationUtils.loadAnimation(ChatWithAllActivity.this, R.anim.button_click_anim);
    }

    private void setupRecyclerView() {
        chatAdapter = new ChatWithAllAdapter(this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setStackFromEnd(true);
        rclView_Chat.setLayoutManager(mLayoutManager);
        rclView_Chat.setItemAnimator(new DefaultItemAnimator());
        rclView_Chat.setHasFixedSize(true);
        rclView_Chat.setAdapter(chatAdapter);
    }

    private void getAllChats() {
        Log.v("GetAllChats", "getAllChats ");
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                APIs.GROUP_CHAT + getString(R.string.group_id) + "/messages" + APIs.API_KEY,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("GetAllChatsOfAGroup", "Response: " + response.toString());
                        ArrayList<ChatModel> newChat = new ArrayList<>();
                        if (response.length() > 0) {
                            Iterator<String> iterator = response.keys();
                            while (iterator.hasNext()) {
                                String messageId = iterator.next();

                                Log.v("GetAllChats", "Iterator ID: " + messageId);
                                try {
                                    JSONObject object1 = response.getJSONObject(messageId);
                                    String message = object1.getString("message");
                                    Log.v("GetAllChats", "Message" + message);
                                    String nameOfReciver = object1.getString("nameOfReciver");
                                    String time = object1.getString("time");
                                    String senderId = object1.getString("senderId");

                                    if (!senderId.equals(preferences.getUserId())) {
                                        continue;
                                    }

                                    ChatModel model = new ChatModel(message, nameOfReciver, time, senderId);
                                    model.setMessageId(messageId);
                                    newChat.add(model);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.v("GetAllChats", "Exception: " + e.getMessage());
                                }

                            }

                            if (previousChat.size() == 0 && newChat.size() != 0) {
                                previousChat = newChat;
                                chatAdapter.setData(previousChat);
                                rclView_Chat.setAdapter(chatAdapter);
                            } else if (newChat.size() > previousChat.size()) {
                                int insertIndex = previousChat.size();
                                for (int i = previousChat.size(); i < newChat.size(); i++) {
                                    previousChat.add(newChat.get(i));
                                    chatAdapter.notifyItemRangeInserted(insertIndex, previousChat.size());
                                    rclView_Chat.setAdapter(chatAdapter);
                                    chatAdapter.notifyItemRangeInserted(insertIndex, previousChat.size());
                                }
                            }
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.v("GetAllChats", "Error: " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_Menu:
                break;
            case R.id.iv_Back:
                iv_Back.startAnimation(scale);
                finish();
                break;
            case R.id.iv_Send:
                iv_Send.startAnimation(scale);
                sendMessage();
                break;
            case R.id.iv_Attach:
                CropImage.activity()
                        .start(ChatWithAllActivity.this);
                break;

        }
    }

    private void sendMessage() {
        String message = et_Message.getText().toString().trim();
        clearEditText();
        if (!message.equals("")) {
            senMessageToAll(message);
            sendTextMessageToGroup(message);
        }
    }

    private void senMessageToAll(String message) {
        for (int i = 0; i < allUsers.size(); i++) {
            sendMessageToUser(message, allUsers.get(i).getId(), allUsers.get(i).getName());
        }
    }

    private void senImageMessageToAll(Bitmap bitmap) {
        for (int i = 0; i < allUsers.size(); i++) {
            uploadBitmapToUserChat(bitmap, allUsers.get(i).getId(), allUsers.get(i).getName());
        }
    }

    private void sendMessageToUser(final String message, final String id, final String name) {
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                APIs.CHAT + preferences.getUserId() + "/" + id + APIs.API_KEY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        sendNotification(message, id, name);
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("volley", "Error: " + error.getMessage());
                        error.printStackTrace();
                        Log.v("CheckMessageSend", "Error: " + error.getMessage());
                        Toast.makeText(context, "Message sending Failed", Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("message", message);
                params.put("nameOfReciver", name);
                params.put("time", String.valueOf(HelperClass.getTime()));
                params.put("senderId", preferences.getUserId());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void clearEditText() {
        et_Message.setText("");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    senImageMessageToAll(bitmap);
                    uploadBitmapToGroupChat(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void uploadBitmapToUserChat(final Bitmap bitmap, final String id, final String name) {
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                APIs.CHAT + preferences.getUserId() + "/" + id + APIs.API_KEY,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        if (response.data.length > 0) {
                            Log.v("CheckMessageSend", "Response: " + response);
                            try {
                                JSONObject object = new JSONObject(new String(response.data));
                                String message = object.getString("message");
                                String nameOfReciver = object.getString("nameOfReciver");
//                                String time = object.getString("time");
//                                String senderId = object.getString("senderId");
                                sendNotification(message, id, nameOfReciver);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("nameOfReciver", name);
                params.put("time", String.valueOf(HelperClass.getTime()));
                params.put("senderId", preferences.getUserId());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("message", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    private void uploadBitmapToGroupChat(final Bitmap bitmap) {
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                APIs.GROUP_CHAT + getString(R.string.group_id) + "/messages" + APIs.API_KEY,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
//                        if (response.data.length > 0) {
//                            Log.v("CheckMessageSend", "Response: " + response);
//                            try {
//                                JSONObject object = new JSONObject(new String(response.data));
//                                String message = object.getString("message");
//                                String nameOfReciver = object.getString("nameOfReciver");
//                                String time = object.getString("time");
//                                String senderId = object.getString("senderId");
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("nameOfReciver", getString(R.string.group_name));
                params.put("time", String.valueOf(HelperClass.getTime()));
                params.put("senderId", preferences.getUserId());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("message", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void sendTextMessageToGroup(final String message) {
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                APIs.GROUP_CHAT + getString(R.string.group_id) + "/messages" + APIs.API_KEY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (!response.isEmpty()) {
                            Log.v("CheckMessageSend", "Response: " + response);
                            try {
                                JSONObject object = new JSONObject(response);
                                String message = object.getString("message");
                                String nameOfReciver = object.getString("nameOfReciver");
                                String time = object.getString("time");
                                String senderId = object.getString("senderId");
                                ChatModel model = new ChatModel(message, nameOfReciver, time, senderId);
                                if (previousChat.size() == 0) {
                                    previousChat.add(model);
                                    chatAdapter.setData(previousChat);
                                    rclView_Chat.scrollToPosition(chatAdapter.getItemCount() - 1);
                                } else {
                                    if (!previousChat.get(previousChat.size() - 1).getTime().equals(model.getTime()))
                                        previousChat.add(model);
                                    chatAdapter.notifyDataSetChanged();
                                    rclView_Chat.scrollToPosition(chatAdapter.getItemCount() - 1);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("volley", "Error: " + error.getMessage());
                        error.printStackTrace();
                        Log.v("CheckMessageSend", "Error: " + error.getMessage());
                        Toast.makeText(context, "Message sending Failed", Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("message", message);
                params.put("nameOfReciver", getString(R.string.group_name));
                params.put("time", String.valueOf(HelperClass.getTime()));
                params.put("senderId", preferences.getUserId());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(r);
    }

    private void sendNotification(final String message, String receiverId, final String receiverName) {
        Log.v("CheckNotifySend", "API: " + APIs.SEND_NOTIFICATION + "/" + receiverId + "/" + preferences.getUserId() + APIs.API_KEY);
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                APIs.SEND_NOTIFICATION + receiverId + "/" + preferences.getUserId() + APIs.API_KEY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (!response.isEmpty()) {
                            Log.v("CheckNotifySend", "Response: " + response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("volley", "Error: " + error.getMessage());
                        error.printStackTrace();
                        Log.v("CheckNotifySend", "Error: " + error.getMessage());
                        Toast.makeText(context, "Notification sending Failed", Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nameOfReciver", receiverName);
                params.put("time", String.valueOf(HelperClass.getTime()));
                params.put("senderId", preferences.getUserId());
                params.put("message", message);
                params.put("senderName", preferences.getName());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
